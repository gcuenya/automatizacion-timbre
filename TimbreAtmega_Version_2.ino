#include <RTClib.h>

// Date and time functions using a DS1307 RTC connected via I2C and Wire lib

#include <Wire.h>
#include <LiquidCrystal.h>

//const int buttonPin = 2;     // PIN DE ENTRADA PARA CONTROL DEL TOUCH
//int buttonState = 0;
int Tiempo;
int hour;
int minute;
int second;
int month;
 int day;
int dayOfWeek;
int dayOfMonth;
int year;
long startTime;
boolean recreo = LOW;
boolean camhora = LOW;
boolean hr_sync = HIGH;							// HIGH = Programa hora en el RTC; LOW = No programa
int rele = 6;									// PIN DE SALIDA PARA CONTROL DEL RELE
LiquidCrystal lcd (7, 8, 9, 10, 11 , 12);		// 7= RS.   8= E.  9= D4.   10= D5.   11= D6.   12= D7.

////////////////////////////////////////////
//	DEINICION DE HORA Y MINUTOS DE CADA RECREO
//	Y DURACION DEL SONIDO DEL TIMBRE
////////////////////////////////////////////
int R0_H = 16;	int R0_M = 20;			// HORA DE ENTRADA
int R1_H = 16;	int R1_M = 21;			// HORA DEL 1� CAMBIO DE HORARIO
int R2_H = 8;	int R2_M = 54;			// HORA DEL 1� RECREO
int R3_H = 9;	int R3_M = 5;			// HORA DE FIN DEL RECREO
int R4_H = 9;	int R4_M = 45;			// HORA DEL 2� CAMBIO DE HORARIO
int R5_H = 10;	int R5_M = 25;			// HORA DEL 2� RECREO
int R6_H = 10;  int R6_M = 35;			// HORA DE FIN DEL RECREO
int R7_H = 11;  int R7_M = 15;			// HORA DEL 3� CAMBIO DE HORARIOHORA DE ENTRADA
int R8_H = 11;  int R8_M = 55;			// HORA DEL 4� CAMBIO DE HORARIO
int R9_H = 12;  int R9_M = 30;			// ALMUERZO
int R10_H = 13; int R10_M = 30;			// FIN DEL ALMUERZO
int R11_H = 14; int R11_M = 10;			// HORA DEL 5� CAMBIO DE HORARIO
int R12_H = 14; int R12_M = 50;			// HORA DEL 3� RECREO
int R13_H = 15; int R13_M = 00;			// HORA DE FIN DEL RECREO
int R14_H = 15; int R14_M = 40;			// HORA DEL 6� CAMBIO DE HORARIO
int R15_H = 16; int R15_M = 20;			// FIN DEL D�A

int cantTimb = 3;				// CANTIDAD DE TIMBRES QUE MARCAN EL CAMBIO DE HORA
long delay_rec = 6000;			// 6''
long delay_cah = 2000;			// 3'' (LA MITAD PRENDIDO Y LA MITAD APAGADO)
////////////////////////////////////////////

RTC_DS1307 rtc;

void setup () {
	lcd.begin(20,4);
	pinMode(buttonPin, INPUT);
	pinMode(rele,OUTPUT);
	digitalWrite (rele, LOW);
	Serial.begin(9600);
        attachInterrupt(0 , get_time , RISING);
	
        #ifdef AVR
		Wire.begin();
	#else
		Wire.begin(); // Shield I2C pins connect to alt I2C bus on Arduino Due
	#endif
	rtc.begin();
	if(hr_sync)
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));		// REPROGRAMAR EL RELOJ SEGUN LA HORA DE LA PC
  //              rtc.adjust(DateTime(2015, 2, 8, 14, 18, 00));          //  (AAAA, MM, DD, HH, mm, NS) REPROGRAMAR EL RELOJ SIN NECESIDAD DE LA HORA DE LA PC
}



void loop () {
	get_time();			// SE CARGAN LOS VALORES DE HR:MI: y DAYOFWEEK

// HABILITACION DE TIMBRES
	
	if (dayOfWeek >=1 && dayOfWeek <6){					// SI NO ES FIN DE SEMANA
		if (hour == R0_H && minute == R0_M && second < 6){			// IZADO DE BANDERA 7:30
			recreo = HIGH;
		}
		if (hour == R1_H && minute == R1_M && second < 6){			// 1� CAMBIO DE HORA.
			camhora = HIGH;
		}		
		if (hour == R2_H && minute == R2_M && second < 6){			// 1� RECREO
			recreo = HIGH;
		}
		if (hour == R3_H && minute == R3_M && second < 6){			// FIN RECREO
			camhora = HIGH;
		}
		if (hour == R4_H && minute == R4_M && second < 6){			// 2� CAMBIO DE HORA
			camhora = HIGH;
		}
		if (hour == R5_H && minute == R5_M && second < 6){			// 2� RECREO
			recreo = HIGH;
		}
		if (hour == R6_H && minute == R6_M && second < 6){			// FIN RECREO 
			recreo = HIGH;
		}
		if (hour == R7_H && minute == R7_M && second < 6){			// 3� CAMBIO DE HORA
			camhora = HIGH;
		}	
		if (hour == R8_H && minute == R8_M && second < 6){			// 4� CAMBIO DE HORA
			camhora = HIGH;
		}
		if (hour == R9_H && minute == R9_M && second < 6){			// ALMUERZO
			recreo = HIGH;
		}
		if (hour == R10_H && minute == R10_M && second < 6){			// FIN ALMUERZO
			recreo = HIGH;
		}
		if (hour == R11_H && minute == R11_M && second < 6) {			// 5� CAMBIO DE HORA
			camhora = HIGH;
		}	
		if (hour == R12_H && minute == R12_M && second < 6){			// 3� RECREO
			recreo = HIGH;
		}
		if (hour == R13_H && minute == R13_M && second < 6){			// FIN RECREO
			recreo = HIGH;
		}
		if (hour == R14_H && minute == R14_M  && second < 6){			// HORA DEL 6� CAMBIO DE HORA
			camhora = HIGH;
		} 
		if (hour == R15_H && minute == R15_M && second < 6){			// FIN DEL DIA
			recreo = HIGH;

		}
	}
// SI LA BANDERA DEL RECREO ESTA EN ALTO ENERGIZO EL RELE DURANTE 6 SEGUNDOS
		if (recreo == HIGH){
			//digitalWrite (rele, HIGH);            // PARA CAMAPANA
			analogWrite(rele,2);                  // PARA PRUEBA
                        startTime = millis();
			while (millis()-startTime <= delay_rec){
				delay(100);
get_time();
			}
			digitalWrite (rele, LOW);            // PARA CAMPANA
			recreo = LOW;					// LIMPIO LA BANDERA DEL TIMBRE PARA EL PROXIMO
		}
		if (camhora == HIGH){
			for(int i = 0; i < cantTimb; i++){
				//digitalWrite (rele, HIGH);            // PARA CAMPANA
				analogWrite(rele,50);                  // PARA PRUEBA

                                startTime = millis();
				while (millis()-startTime < delay_cah){
					delay(100);
get_time();
				}
				digitalWrite (rele, LOW);

				startTime = millis();
				while (millis()-startTime < delay_cah){
					delay(100);
get_time();
				}
			}
			camhora = LOW;
		}

	delay(100);				//ESPERA ENTRE LECTURAS DEL RELOJ (RTC)
} ////// FIN PROGRAMA PRINCIPAL ///////


////// SUBRUTINAS //////
void get_time() {
	DateTime now = rtc.now();
        year = now.year();
	month = now.month();
	day = now.day();
	hour = now.hour();
	minute = now.minute();
	second = now.second();
	dayOfWeek = now.dayOfWeek();

        //MUESTRA DE HORA EN DISPLAY
        lcd.setCursor(3,1);
        lcd.print("HORA: ");
        lcd.print(hour);
        Serial.print(hour);
        lcd.print(":");
        Serial.print(":");
        lcd.print(minute);
        Serial.print(minute);
        lcd.print(":");
        Serial.print(":");
        lcd.print(second);
        Serial.println(second);
        lcd.print(".");
        delay(10);
       
         //MUESTRA DE FECHA EN DISPLAY
        lcd.setCursor(2,0);
        lcd.print("FECHA: ");
        lcd.print(day);
        lcd.print("/");
        lcd.print(month);
        lcd.print("/");
        lcd.print(year);
        delay(10);
                        	  
}
